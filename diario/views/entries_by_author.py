# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.http import Http404
from django.utils.translation import ugettext as _
from django.views.generic.list import ListView

from diario.settings import DIARIO_NUM_LATEST
from diario.models import Entry


class EntryListByAuthor(ListView):
    paginate_by = DIARIO_NUM_LATEST
    template_name = 'diario/entry_list_by_author.html'
    
    def dispatch(self, request, *args, **kwargs):
        self.author = kwargs.get('author', None)
        if not self.author:
            raise AttributeError(_('entry_list_by_author must be called with '
                                   'a author.'))
        return super(EntryListByAuthor, self).dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        try:
            self.user = User.objects.get(username=self.author)
        except User.DoesNotExist:
            raise Http404(_('No User found matching "%s".') % self.author)
        return Entry.published_on_site.filter(author=self.user)
    
    def get_context_data(self, **kwargs):
        context = super(EntryListByAuthor, self).get_context_data(**kwargs)
        context['author'] = self.user
        return context
  
