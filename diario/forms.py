# -*- coding: utf-8 -*-

from django import forms
from django.utils import formats
from django.utils.translation import ugettext_lazy as _


class EntryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EntryForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            if self.instance.is_visible():
                date = self.instance.published_at
                help_text = _('Published at {}')
            else:
                date = self.instance.updated_at
                help_text = _('Last updated at {}')
            fmt_date = formats.date_format(date, 'SHORT_DATETIME_FORMAT')
            self.fields['status'].help_text = help_text.format(fmt_date)
