# -*- coding: utf-8 -*-

"""Default Diário application settings.

If you do not configure the settings below in your own project settings.py,
they will assume default values::

    DIARIO_ALLOW_COMMENTS
        Default value for ``Entry.allow_comments`` boolean field.
        Default: True

    DIARIO_PAGINATE_BY
        Number of itens per page on list and archive views. Default: 12

    DIARIO_STATUS_CHOICES
        List of choices for the field ``Entry.status``.

    DIARIO_VISIBLE_STATUS
        Entries with these status choices are considered visible on
        the web. *Used by ``EntryManager.visible()`` method*.
        Default: ('published',)

"""

from django.conf import settings
from django.utils.translation import ugettext_lazy as _


ALLOW_COMMENTS = getattr(settings, 'DIARIO_ALLOW_COMMENTS', True)
PAGINATE_BY = getattr(settings, 'DIARIO_PAGINATE_BY', 12)
STATUS_CHOICES = getattr(settings, 'DIARIO_STATUS_CHOICES', (
    # ('archived', _('Archived')),
    ('draft', _('Draft')),
    # ('review', _('To review')),
    # ('scheduled', _('Scheduled')),
    ('published', _('Published')),
))
VISIBLE_STATUS = getattr(settings, 'DIARIO_VISIBLE_STATUS', (
    'published',
))
