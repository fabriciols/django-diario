# -*- coding: utf-8 -*-

from django.contrib.sitemaps import Sitemap
from diario.models import Entry


class DiarioSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Entry.published_on_site.all()

    def lastmod(self, obj):
        return obj.pub_date
