# -*- coding: utf-8 -*-

"""Default Diário application settings.

If you do not configure the settings below in your own project settings.py,
they will assume default values::

    DIARIO_MARKUP_CHOICES
        Avaialable choices of markup languages for a entry.
        Default: (('markdown', 'Markdown'), ('rest', 'reStructuredText'),
                  ('textile',  'Textile'),  ('raw', _('Raw text')))

    DIARIO_DEFAULT_MARKUP_LANG
        Default markup language for blog entries.
        Options: 'rest', 'textile', 'markdown' or 'raw' for raw text.
        Default: 'raw'.

    DIARIO_NUM_LATEST
        Number of latest itens on object_list view. Default: 10.

"""

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

#
#  (!!!)
#
#  DON'T EDIT THESE VALUES, CONFIGURE IN YOUR OWN PROJECT settings.py
#

DIARIO_MARKUP_CHOICES = getattr(settings, 'DIARIO_MARKUP_CHOICES', (
    ('markdown', 'Markdown'),
    ('rest',     'reStructuredText'),
    ('textile',  'Textile'),
    ('raw',      _('Raw text')),
))
DIARIO_DEFAULT_MARKUP_LANG = getattr(settings, 'DIARIO_DEFAULT_MARKUP_LANG', 'raw')
DIARIO_NUM_LATEST = getattr(settings, 'DIARIO_NUM_LATEST', 10)


# django-tagging support
HAS_TAG_SUPPORT = False
