# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'diario.views',
    url('^(?P<username>[^/]+)/$', 'user', name='user'),
    url('^(?P<username>[^/]+)/feed/$', 'user_feed', name='user-feed'),
)
