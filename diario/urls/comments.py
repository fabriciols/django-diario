# -*- coding: utf-8 -*-

"""URL definitions for weblog comments"""

from django.conf.urls.defaults import *
from django.contrib.comments.models import Comment
from django.contrib.sites.models import Site

from diario import feeds


def get_comments():
    queryset = Comment.objects.filter(
        site=Site.objects.get_current(),
        content_type__app_label__exact='diario',
        content_type__model__exact='entry'
    )
    return queryset.order_by('-submit_date')


comments = url(
    regex  = '^$',
    view   = 'django.views.generic.list_detail.object_list',
    name   = 'diario-comments',
    kwargs = {
        'queryset': get_comments(),
        'paginate_by': 15,
    }
)


comments_atom = url(
    regex = 'atom/$',
    view  = feeds.comments.AtomCommentsFeed(),
    name  = 'diario-comments-atom'
)

comments_rss = url(
    regex = 'rss/$',
    view  = feeds.comments.RssCommentsFeed(),
    name  ='diario-comments-rss'
)


urlpatterns = patterns('',
    comments,
    comments_atom, comments_rss
)
