# -*- coding: utf-8 -*-

"""URL definitions for weblog entries divided by author"""

from django.conf.urls.defaults import *

from diario.feeds import entries_by_author as feeds
from diario.views import entries_by_author as views


# TODO: AuthorListView
# author_list = url(
#     regex  = '^$',
#     view   = views.AuthorListView.as_view(),    
#     name   = 'diario-author-list',
# )

entry_list_by_author = url(     # entries by author
    regex  = '^(?P<author>[^/]+)/$',
    view   = views.EntryListByAuthor.as_view(),    
    name   = 'diario-author-entry-list',
)


entries_by_author_atom = url(
    regex = '^(?P<author>[^/]+)/atom/$',
    view  = feeds.AtomEntriesByAuthorFeed(),
    name  = 'diario-author-entry-atom'
)

entries_by_author_rss = url(
    regex = '^(?P<author>[^/]+)/rss/$',
    view  = feeds.RssEntriesByAuthorFeed(),
    name  = 'diario-author-entry-rss'
)


urlpatterns = patterns('',
    entry_list_by_author,
    entries_by_author_atom, entries_by_author_rss
)
