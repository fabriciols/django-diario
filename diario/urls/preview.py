# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'diario.views',
    url('^(?P<pk>\d+)/$', 'entry_preview', name='entry-preview'),
)
