# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'diario.views',
    url('^(?P<slug>[-\w]+)/$', 'tag', name='tag')
)
