# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'diario.views',
    url('^$', 'archive_index', name='archive-index'),
    url('^(?P<year>\d{4})/$', 'year_archive', name='archive-year'),
    url('^(?P<year>\d{4})/(?P<month>\d{2})/$', 'month_archive', name='archive-month'),
    url('^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$', 'day_archive', name='archive-day'),
)
