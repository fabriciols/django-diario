=============
Django Diário
=============

*(django-diario is no longer supported!)*

Django Diário is a pluggable weblog application for `Django Web Framework`_

Project page
    https://bitbucket.org/semente/django-diario/
Translations
    https://www.transifex.net/projects/p/django-diario/

.. _`Django Web Framework`: http://www.djangoproject.com



License
=======

Copyright (c) 2013 Guilherme Gondim and individual contributors

Django Diário is free software; you can redistribute it
and/or modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

Django Diário is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; see the file COPYING.LESSER. If not,
see <http://www.gnu.org/licenses/>.