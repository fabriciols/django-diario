# -*- coding: utf-8 -*-
# Django settings for example project.

from os.path import join, abspath, dirname


PROJECT_ROOT = abspath(dirname(dirname(__file__)))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (('root', 'root@localhost'),)
MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(PROJECT_ROOT, 'sqlite3.db'),
    }
}

SITE_ID = 1
ALLOWED_HOSTS = ['127.0.0.1', 'localhost']
SECRET_KEY = 'make-this-unique,-and-dont-share-it-with-anybody.'

TIME_ZONE = 'America/Chicago'
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = join(PROJECT_ROOT, '_assets', 'media')
MEDIA_URL = '/media/'
STATIC_ROOT = join(PROJECT_ROOT, '_assets', 'static')
STATIC_URL = '/static/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'example.urls'
WSGI_APPLICATION = 'example.wsgi.application'

STATICFILES_DIRS = [join(PROJECT_ROOT, 'static')]
TEMPLATE_DIRS = [join(PROJECT_ROOT, 'templates')]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'south',
    'taggit',
    'diario',
)


# django-diario
DIARIO_ALLOW_COMMENTS = True
#DIARIO_LEAD_SIZE (use default: 25)
#DIARIO_PAGINATE_BY (use default: 12)
#DIARIO_STATUS_CHOICES (use default)
#DIARIO_VISIBLE_STATUS (use default)
