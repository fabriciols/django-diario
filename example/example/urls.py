from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()


blog_urls = patterns(
    '',
    url(r'^', include('diario.urls')),
    url(r'^archive/', include('diario.urls.archive')),
    url(r'^category/', include('diario.urls.categories')),
    url(r'^tag/', include('diario.urls.tags')),
    url(r'^people/', include('diario.urls.people')),
)

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(blog_urls,  namespace='blog', app_name='diario')),
)
